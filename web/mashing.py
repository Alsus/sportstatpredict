import pandas as pd
import numpy as np
import collections
from sklearn.linear_model import LinearRegression
import csv


def get_season_team_stat(team_id):
    with open('football_stats.csv', 'r', encoding='utf-8') as file:
        reader = csv.reader(file)
        next(reader)
        for row in reader:
            if int(row[0]) == team_id:
                return [row[1], row[2], row[3], row[4], row[5],
                        row[6], row[7], row[8], row[9], row[10],
                        row[11], row[12], row[13], row[14], row[15]]


def get_season_all_team_stat(teams):
    annual = collections.defaultdict(list)
    for team in teams:
        team_vector = get_season_team_stat(team.id)
        annual[team] = team_vector
    return annual


def get_training_data(teams):
    total_num_games = 0
    for team in teams:
        total_num_games += team.matches

    num_features = len(get_season_team_stat(4499))
    x_train = np.zeros((total_num_games, num_features))
    y_train = np.zeros((total_num_games))
    index_counter = 0

    team_vectors = get_season_all_team_stat(teams)
    annual = data
    num_games = len(annual.index) * 29
    x_train_annual = np.zeros((num_games, num_features))
    y_train_annual = np.zeros((num_games))
    counter = 0

    for index, row in annual.iterrows():
        team = row['Команда']
        t_vector = team_vectors[team]
        rivals = row['Соперник']
        r_vector = team_vectors[rivals]
        diff = [a - b for a, b in zip(t_vector, r_vector)]

        if len(diff) != 0:
            x_train_annual[counter] = diff
        if team == row['Победитель']:
            y_train_annual[counter] = 1
        else:
            y_train_annual[counter] = 0
        counter += 1

    x_train[index_counter:num_games + index_counter] = x_train_annual
    y_train[index_counter:num_games + index_counter] = y_train_annual
    index_counter += num_games

    return x_train, y_train


def create_game_prediction(team1_vector, team2_vector):
    diff = [[a - b for a, b in zip(team1_vector, team2_vector)]]

    predictions = model.predict(diff)
    return predictions


data = pd.read_csv('football_stats.csv', encoding='cp1251', delimiter=',')
data.head()

team_list = []

with open('football_stats.csv', 'r', encoding='utf-8') as file:
    reader = csv.reader(file)
    for row in reader:
        if row:
            team_list.append(row[1])

xTrain, yTrain = get_training_data(team_list)

model = LinearRegression()
model.fit(xTrain, yTrain)

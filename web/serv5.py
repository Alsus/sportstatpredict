import csv
import re

import requests


class TeamStat:
    def __init__(self, id, name):
        self.id = id
        self.name = name


class ForTable:
    def __init__(self, team, opponent, winner):
        self.team = team
        self.opponent = opponent
        self.winner = winner


def create_team(id, name):
    team = TeamStat(
        id=id,
        name=name,
    )
    return team


def get_teams():
    url = 'http://livescore-api.com/api-client/teams/list.json'
    params = {
        'key': 'mhuNhwSBrPnkuJUl',
        'secret': 'N7fKk2NJPPwOlGKIYevfLoRdeexIXs4K',
        'country_id': 12
    }
    response = requests.get(url, params=params)

    response = response.json()

    list = []

    for i in range(len(response['data']['teams'])):

        if len(response['data']['teams'][i]['translations']) > 0:
            list.append(
                create_team(response['data']['teams'][i]['id'], response['data']['teams'][i]['translations']['ru']))
        else:
            list.append(
                create_team(response['data']['teams'][i]['id'], response['data']['teams'][i]['name']))

    return list


def get(id, teams):

    url = 'http://livescore-api.com/api-client/scores/history.json'
    params = {
        'key': 'mhuNhwSBrPnkuJUl',
        'secret': 'N7fKk2NJPPwOlGKIYevfLoRdeexIXs4K',
        'team': id
    }

    response = requests.get(url, params=params)

    ret_data = []

    if response.status_code == 200:

        data = response.json()
        command = ''
        opponent = ''

        for match in data['data']['match']:
            flag1 = False
            flag2 = False

            score = re.findall(r'\d+', match['score'])
            if int(score[0]) > int(score[1]):
                winner = command
            else:
                winner = opponent

            ret_data.append(ForTable(command, opponent, winner))

            for team in teams:

                if int(match['home_id']) == id:
                    flag1 = True
                    command = team.id

                elif int(match['away_id']) == team.id:
                    flag2 = True
                    opponent = team.id

            print(flag1, flag2)
            if flag1 & flag2:
                score = re.findall(r'\d+', match['score'])
                if int(score[0]) > int(score[1]):
                    winner = command
                else:
                    winner = opponent

                ret_data.append(ForTable(command, opponent, winner))

    return ret_data


def write_csv():

    headers = ['Команда', 'Соперник', 'Победитель']

    data = get_teams()

    print('We Start')
    with open('matches.csv', 'w', encoding='utf-8') as file:
        writer = csv.writer(file)
        writer.writerow(headers)

        for team in data:
            matches = get(team.id, data)

            for match in matches:
                list = [match.team, match.opponent, match.winner]
                writer.writerow(list)

    print("Таблица успешно создана и записана в файл")


print('WE STARr')
write_csv()

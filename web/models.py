from django.db import models

from django.contrib.auth import get_user_model

User = get_user_model()


class Team(models.Model):
    name = models.CharField(max_length=255)
    emblem = models.CharField(max_length=255)
    city = models.CharField(max_length=255)


class Coach(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    experience = models.CharField(max_length=255)
    team = models.ForeignKey(Team, on_delete=models.SET_NULL, null=True, verbose_name="Команда")


class Game(models.Model):
    game_data = models.DateTimeField(verbose_name='Время игры', null=True, blank=True)
    place = models.CharField(max_length=255)
    score = models.CharField(max_length=100)
    completed = models.BooleanField()
    game_time = models.CharField(max_length=255)
    guest = models.ForeignKey(Team, on_delete=models.CASCADE, null=True, verbose_name="Гости", related_name="game_guest_team")
    host = models.ForeignKey(Team, on_delete=models.CASCADE, null=True, verbose_name="Хозяева", related_name="game_host_team")


class Player(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    age = models.CharField(max_length=50)
    height = models.CharField(max_length=50)
    weight = models.CharField(max_length=50)
    experience = models.CharField(max_length=255)
    position = models.CharField(max_length=100)
    country = models.CharField(max_length=255)
    team = models.ForeignKey(Team, on_delete=models.SET_NULL, null=True, verbose_name="Команда")


class Goal(models.Model):
    minute = models.CharField(max_length=100)
    penalty = models.BooleanField()
    player = models.ForeignKey(Player, on_delete=models.CASCADE, null=True)
    game = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class Replacement(models.Model):
    minute = models.CharField(max_length=100)
    who = models.ForeignKey(Player, on_delete=models.CASCADE, null=True, verbose_name="Кто заменен", related_name="rep_who_player")
    whom = models.ForeignKey(Player, on_delete=models.CASCADE, null=True, verbose_name="На кого замена", related_name="rep_whom_player")
    game = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)


class Card(models.Model):
    minute = models.CharField(max_length=100)
    card = models.IntegerField()
    player = models.ForeignKey(Player, on_delete=models.CASCADE, null=True)
    game = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)








from django.contrib.auth import get_user_model, login, authenticate, logout
from django.core.paginator import Paginator
from django.shortcuts import get_object_or_404
from django.shortcuts import render, redirect

from web.forms import RegistrationForm, AuthForm
from web.models import Team, Coach, Player, Game, Replacement, Card, Goal
from web.services import get_teams_serv, get_matches_main

User = get_user_model()


def registration_view(request):
    form = RegistrationForm()
    is_success = False
    if request.method == 'POST':
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            user = User(
                username=form.cleaned_data['username'],
                email=form.cleaned_data['email'],
            )
            user.set_password(form.cleaned_data['password'])
            user.save()
            is_success = True
            return redirect("auth")
    return render(request, "web/index_reg.html", {
        "form": form, "is_success": is_success
    })


def auth_view(request):
    form = AuthForm()
    if request.method == 'POST':
        form = AuthForm(data=request.POST)
        if form.is_valid():
            user = authenticate(**form.cleaned_data)
            if user is None:
                form.add_error(None, "Введены неверные данные")
            else:
                login(request, user)
                return redirect("main")
    return render(request, "web/index_auth.html", {"form": form})


def get_teams(request):
    return render(request, "web/teams.html", {"teams": get_teams_serv()})


def get_team_main(request, id):
    team = get_object_or_404(Team, id=id)
    coach = get_object_or_404(Coach, team_id=id)
    players = Player.objects.filter(team_id=id)

    data1 = get_matches_main()
    games_compl = []
    games_non_compl = []
    for game in data1:
        if game.score != '-:-' and (team.name == game.name1 or team.name == game.name2):
            games_compl.append(game)
        elif game.score == '-:-' and (team.name == game.name1 or team.name == game.name2):
            games_non_compl.append(game)

    return render(request, "web/team_main.html", {
        "team": team,
        "coach": coach,
        "players": players,
        "games_completed": games_compl,
        "games_non_completed": games_non_compl,
    })


def get_team_constituent(request, id):
    team = get_object_or_404(Team, id=id)
    coach = get_object_or_404(Coach, team_id=id)
    players = Player.objects.filter(team_id=id).order_by('position')

    return render(request, "web/team_constituent.html", {
        "team": team,
        "coach": coach,
        "players": players
    })


def get_all_completed_games(request):
    data = get_matches_main()
    new_data = []
    for game in data:
        if game.score != '-:-':
            new_data.append(game)

    paginator = Paginator(new_data, per_page=4)

    page_number = request.GET.get("page", 1)
    return render(request, "web/games_completed.html", {
        "games_completed": paginator.get_page(page_number)
    })


def get_all_non_completed_games(request):
    data = get_matches_main()
    new_data = []
    for game in data:
        if game.score == '-:-':
            new_data.append(game)

    paginator = Paginator(new_data, per_page=4)
    page_number = request.GET.get("page", 1)
    return render(request, "web/games_non_completed.html", {
        "games_non_completed": paginator.get_page(page_number)
    })


def get_main(request):
    data = get_matches_main()
    paginator = Paginator(data, per_page=8)

    page_number = request.GET.get("page", 1)

    return render(request, "web/main.html", {
        "games": paginator.get_page(page_number)
    })


def get_lk(request):
    user = User.objects.get(id=request.user.id)
    return render(request, "web/lk.html", {"data": user})


def logout_view(request):
    logout(request)
    return redirect("main")

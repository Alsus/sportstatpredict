from django.urls import path


from web.views import registration_view, auth_view, get_teams, get_team_main, get_team_constituent, \
    get_all_completed_games, get_all_non_completed_games, get_main, get_lk, logout_view

urlpatterns = [
    path("reg/", registration_view, name="reg"),
    path("auth/", auth_view, name="auth"),
    path("logout/", logout_view, name="logout"),
    path("teams/", get_teams, name="teams"),
    path("team_main/<int:id>", get_team_main, name="team_main"),
    path("team_constituent/<int:id>", get_team_constituent, name="team_constituent"),
    path("games_completed", get_all_completed_games, name='games_completed'),
    path("games_non_completed", get_all_non_completed_games, name='games_non_completed'),
    path("", get_main, name='main'),
    path("lk", get_lk, name="lk")

]

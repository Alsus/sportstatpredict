import csv

from web.models import Team


class Game:
    def __init__(self, date, name1, name2, score, per1, per2, per3):
        self.date = date
        self.name1 = name1
        self.name2 = name2
        self.score = score
        self.per1 = per1
        self.per2 = per2
        self.per3 = per3
        self.picture1 = name1 + '.jpg'
        self.picture2 = name2 + '.jpg'


def get_teams_serv():
    return Team.objects.all()


def get_matches_main():
    data = []
    with open('MatchesMain.csv', 'r', encoding='utf-8') as file:
        reader = csv.reader(file)

        for row in reader:
            data.append(Game(row[0], row[1], row[2], row[3], row[4], row[5], row[6]))

    return data

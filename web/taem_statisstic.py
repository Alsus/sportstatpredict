import re
import csv
from urllib.parse import parse_qs
from urllib.parse import urlparse

import requests

regex_home = r'(\d+):'
regex_away = r':\s*(\d+)'


class TeamStat:
    goal_scored = 0
    goal_allowed = 0

    game_win = 0
    game_draw = 0
    game_lost = 0

    total_score = 0

    matches = 0

    shot_of_target = 0
    shot_on_target = 0

    dangerous_attacks = 0
    attacks = 0
    attempts_on_goal = 0

    possesion = 0
    saves = 0

    def __init__(self, id, name):
        self.id = id
        self.name = name

    def set_data(self, data, regex, goal_scored, goal_allowed, flag):
        self.matches += 1
        self.goal_scored += goal_scored
        self.goal_allowed += goal_allowed

        if goal_scored > goal_allowed:
            self.game_win = + 1
            self.total_score += 3
        elif goal_scored < goal_allowed:
            self.game_lost = +1
        else:
            self.game_draw = + 1
            self.total_score += 1

        if flag:
            self.set_more_data(data, regex)

    def set_more_data(self, data, regex):
        try:
            self.shot_on_target += get_data_for_statistic(data['data']['shot_on_target'], regex)
            self.shot_of_target += get_data_for_statistic(data['data']['shot_of_target'], regex)
            self.dangerous_attacks += get_data_for_statistic(data['data']['dangerous_attacks'], regex)
            self.attacks += get_data_for_statistic(data['data']['attacks'], regex)
            self.possesion += get_data_for_statistic(data['data']['possesion'], regex)
            self.saves += get_data_for_statistic(data['data']['saves'], regex)
            self.attempts_on_goal += get_data_for_statistic(data['data']['attempts_on_goal'], regex)
        except Exception:
            pass

    def print_res(self):
        print(self.matches, self.goal_scored, self.goal_allowed, self.game_win, self.total_score, self.game_lost,
              self.game_draw,
              self.shot_on_target, self.shot_of_target, self.dangerous_attacks, self.attacks, self.possesion,
              self.saves, self.attempts_on_goal)


def create_team(id, name):
    team = TeamStat(
        id=id,
        name=name,
    )
    return team


def get_match_from_event(event):
    parsed_url = urlparse(event)
    match_id = parse_qs(parsed_url.query)['id'][0]
    return match_id


def get_data_for_statistic(data, regex):
    match = re.search(regex, data)
    if match:
        return int(match.group(1))
    else:
        return None


def get_info_by_match(team_id, teams, event, score, regex):
    match_id = get_match_from_event(event)

    url = 'https://livescore-api.com/api-client/matches/stats.json'
    params = {
        'key': 'mhuNhwSBrPnkuJUl',
        'secret': 'N7fKk2NJPPwOlGKIYevfLoRdeexIXs4K',
        'match_id': match_id
    }

    response = requests.get(url, params=params)

    if response.status_code == 200:
        data = response.json()

        team1 = None

        for team in teams:
            if team.id == team_id:
                team1 = team

        score = re.findall(r'\d+', score)
        goal_scored = int(score[0])
        goal_allowed = int(score[1])

        if len(data['data']) > 0:
            team1.set_data(data, regex, goal_scored, goal_allowed, True)
        else:
            team1.set_data(data, regex, goal_scored, goal_allowed, False)
        print("MATCH = ", match_id, len(data['data']))


def get_all_matches_by_team(id, teams):
    url = 'http://livescore-api.com/api-client/scores/history.json'
    params = {
        'key': 'mhuNhwSBrPnkuJUl',
        'secret': 'N7fKk2NJPPwOlGKIYevfLoRdeexIXs4K',
        'team': id
    }

    response = requests.get(url, params=params)

    if response.status_code == 200:

        data = response.json()

        for match in data['data']['match']:
            if match['home_id'] == id:
                get_info_by_match(id, teams, match['events'], match['score'], regex_home)
            else:
                get_info_by_match(id, teams, match['events'], match['score'], regex_away)


def get_teams_from_api():
    url = 'http://livescore-api.com/api-client/teams/list.json'
    params = {
        'key': 'mhuNhwSBrPnkuJUl',
        'secret': 'N7fKk2NJPPwOlGKIYevfLoRdeexIXs4K',
        'country_id': 12
    }
    response = requests.get(url, params=params)

    response = response.json()

    list = []

    for i in range(len(response['data']['teams'])):

        if len(response['data']['teams'][i]['translations']) > 0:
            list.append(
                create_team(response['data']['teams'][i]['id'], response['data']['teams'][i]['translations']['ru']))
        else:
            list.append(
                create_team(response['data']['teams'][i]['id'], response['data']['teams'][i]['name']))

    return list


def write_in_csv():

    headers = ['id', 'name', 'goals_scored', 'goals_allowed', 'game_win', 'game_draw', 'game_lost', 'total_score', 'matches',
               'shot_of_target', 'shot_on_target', 'dangerous_attacks', 'attacks', 'attempts_on_goal', 'possesion', 'saves']

    data = get_teams_from_api()

    for team in data:
        get_all_matches_by_team(team.id, data)

    with open('football_stats.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(headers)
        for team in data:
            team.print_res()
            list = [str(team.id), str(team.name), str(team.goal_scored), str(team.goal_allowed), str(team.game_win), str(team.game_draw),
                    str(team.game_lost), str(team.total_score), str(team.matches), str(team.shot_of_target), str(team.shot_on_target),
                    str(team.dangerous_attacks), str(team.attacks), str(team.attempts_on_goal), str(team.possesion), str(team.saves)]

            writer.writerow(list)

    print("Таблица успешно создана и записана в файл 'football_stats.csv'")



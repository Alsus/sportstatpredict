import pandas as pd
import numpy as np
import collections
from sklearn.linear_model import LinearRegression

model = LinearRegression()


def get_season_team_stat(team, season):
    data = get_data()

    goal_scored = 0  # Голов забито
    goal_allowed = 0  # Голов пропущено

    game_win = 0  # Выиграно
    game_draw = 0  # Ничья
    game_lost = 0  # Проиграно

    total_score = 0  # Количество набранных очков

    matches = 0  # Количество сыгранных матчей

    xG = 0  # Ожидаемые голы

    shot = 0  # Удары
    shot_on_target = 0  # Удары в створ

    cross = 0  # Навесы
    accurate_cross = 0  # Точные навесы

    total_handle = 0  # Владение мячом
    average_handle = 0  # Среднее владение мячом за матч

    pass_count = 0  # Пасы
    accurate_pass = 0  # Точные пасы

    ppda = 0  # Интенсивность прессинга в матче

    for i in range(len(data)):
        if (((data['Год'][i] == season) and (data['Команда'][i] == team) and (data['Часть'][i] == 2)) or (
                (data['Год'][i] == season - 1) and (data['Команда'][i] == team) and (data['Часть'][i] == 1))):
            matches += 1

            goal_scored += data['Забито'][i]
            goal_allowed += data['Пропущено'][i]

            if data['Забито'][i] > data['Пропущено'][i]:
                total_score += 3
                game_win += 1
            elif data['Забито'][i] < data['Пропущено'][i]:
                game_lost += 1
            else:
                total_score += 1
                game_draw += 1

            xG += data['xG'][i]

            shot += data['Удары'][i]
            shot_on_target += data['Удары в створ'][i]

            pass_count += data['Передачи'][i]
            accurate_pass += data['Точные передачи'][i]

            total_handle += data['Владение'][i]

            cross += data['Навесы'][i]
            accurate_cross += data['Точные навесы'][i]

            ppda += data['PPDA'][i]

    average_handle = round(total_handle / matches, 3)  # Владение мячом в среднем за матч

    return [game_win, game_draw, game_lost, goal_scored, goal_allowed, total_score, round(xG, 3), round(ppda, 3), shot,
            shot_on_target, pass_count, accurate_pass, cross, accurate_cross, round(average_handle, 3)]


def get_season_all_team_stat(season):
    team_list = get_team_list()
    annual = collections.defaultdict(list)
    for team in team_list:
        team_vector = get_season_team_stat(team, season)
        annual[team] = team_vector
    return annual


def get_training_data(seasons):
    total_num_games = 0
    data = get_data()

    for season in seasons:
        annual = data[data['Год'] == season]
        total_num_games += len(annual.index)
    num_features = len(get_season_team_stat('Зенит', 2016))  # случайная команда для определения размерности
    x_train = np.zeros((total_num_games, num_features))
    y_train = np.zeros((total_num_games))
    index_counter = 0

    for season in seasons:
        team_vectors = get_season_all_team_stat(season)
        annual = data[data['Год'] == season]
        num_games_in_year = len(annual.index)
        x_train_annual = np.zeros((num_games_in_year, num_features))
        y_train_annual = np.zeros((num_games_in_year))
        counter = 0

        for index, row in annual.iterrows():
            team = row['Команда']
            t_vector = team_vectors[team]
            rivals = row['Соперник']
            r_vector = team_vectors[rivals]

            diff = [a - b for a, b in zip(t_vector, r_vector)]

            if len(diff) != 0:
                x_train_annual[counter] = diff
            if team == row['Победитель']:
                y_train_annual[counter] = 1
            else:
                y_train_annual[counter] = 0
            counter += 1
        x_train[index_counter:num_games_in_year + index_counter] = x_train_annual
        y_train[index_counter:num_games_in_year + index_counter] = y_train_annual
        index_counter += num_games_in_year
    return x_train, y_train


def create_game_prediction(team1_vector, team2_vector):
    diff = [[a - b for a, b in zip(team1_vector, team2_vector)]]
    predictions = model.predict(diff)
    return predictions


def get_data():
    data = pd.read_csv("C:\ProjectsFromGIT\sportstatpredict\RPL.csv", encoding='cp1251', delimiter=';')
    data.head()
    RPL_2018_2019 = pd.read_csv('../Teams.csv', encoding='cp1251')
    teamList = RPL_2018_2019['Team Name'].tolist()

    deleteTeam = [x for x in pd.unique(data['Соперник']) if x not in teamList]
    for name in deleteTeam:
        data = data[data['Команда'] != name]
        data = data[data['Соперник'] != name]
    data = data.reset_index(drop=True)

    return data


def get_team_list():
    data = pd.read_csv("C:/ProjectsFromGIT/sportstatpredict/RPL.csv", encoding='utf-8', delimiter=';')
    data.head()
    RPL_2018_2019 = pd.read_csv('C:/ProjectsFromGIT/sportstatpredict/Teams.csv', encoding='utf-8')
    teamList = RPL_2018_2019['Team Name'].tolist()

    return teamList


def get_pred(name):
    team_list = get_team_list()
    prediction = []

    for team_name in team_list:
        team1_name = name
        team2_name = team_name

        if team1_name != team2_name:
            team1_vector = get_season_team_stat(team1_name, 2019)
            team2_vector = get_season_team_stat(team2_name, 2019)

            print([team1_name, team2_name, create_game_prediction(team1_vector, team2_vector),
                               create_game_prediction(team2_vector, team1_vector)])

            prediction.append([team1_name, team2_name, create_game_prediction(team1_vector, team2_vector),
                               create_game_prediction(team2_vector, team1_vector)])


years = range(2016, 2019)
xTrain, yTrain = get_training_data(years)

model.fit(xTrain, yTrain)

get_pred("Зенит")